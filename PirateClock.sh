# Short bash script to initialize the hwclock and sync the system time 
# to the hwclock. 

#! /bin/bash
echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
hwclock -s
    