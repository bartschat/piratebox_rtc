How to add a RealTimeClock (DS3231) to your Raspberry Pi powered PirateBox
==========================================================================

History
-------
2016-02-09 Version 1.0 including rewrite for Arch Linux 
2016-02-08 DRAFT version created (Lars@alles-am-en.de)

License stuff
-------------
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 
International License. To view a copy of this license, 
visit http://creativecommons.org/licenses/by-sa/4.0/. 

Intro
-----

This tutorial will describe how your Raspberry Pi powered PirateBox can be 
extended with a RealTimeClock module. Adding an RTC improves your PirateBox 
because the Raspberry Pi does not have any clock on board. So everytime 
your Pi reboots you need some method to set an exact time or at least some 
reasonable estimate. When your Raspberry Pi is in any way connected to the 
Internet this is trivial, just setup NTP (network time protocol) and the Pi 
will sync it´s system time to the configured NTP server. 
As a PirateBox´s purpose is to provide "anonymous offline sharing and 
communication" a connection to the internet is usually either not wanted 
or not possible. There are some workaraounds that involve the use of 
files´ last modified dates and stuff to set something of an assumption of 
the current time, but these are all more or less educated guesses. A cheap 
and easy way to deal with this issue is to add a DS3231 RTC to your setup. 
You can get these modules for as low as 2 or 3 US$ from China based retailers 
or for a little more if you want to order at local retailers. 
So, let´s get started and add timekeeping to your PirateBox.



Step 1 - Buy RTC module
-----------------------
Acquire a RealTimeClock module with a DS3231 chip at a retailer of your choice. 
DS1307 chips are working fine as well, they just are not as precise as the 
DS3231 modules. 



Step 2 - Add I2C support
------------------------
* Make sure your system is up to date:

                $ sudo pacman -Syu


* Install I2C packages:
* 
                $ sudo pacman -S i2c-tools


* Activate I2C support in `config.txt`:

                $ sudo vim /boot/config.txt
                
                
* uncomment the following line
                
                device_tree_param=i2c_arm=on


* Load necessary modules on boot:

        $ sudo vim /etc/modules-load.d/raspberrypi.conf
        
        
    * make sure at least the following modules are included:
    
                    snd-bcm2835
                    i2c-bcm2835
                    i2c-dev
                    rtc-ds1307


    * save and power down `sudo shutdown -h now`  


Step 3 - Add RTC module onto GPIO ports
---------------------------------------
This one is really easy, just plug the RTC chip onto the correct GPIO ports. 


Step 4 - Test and set the new hardware clock module
---------------------------------------------------
After the Pi is back up check out if the RTC is detected: 

    $ sudo echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
    $ sudo i2cdetect -y 1
    
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
        00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
        10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
        20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
        30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
        40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
        50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
        60: -- -- -- -- -- -- -- -- UU -- -- -- -- -- -- -- 
        70: -- -- -- -- -- -- -- -- 
        
        
Now make sure that your current system time is correct (via NTP or just set 
it manually with the `date`command). 
Write the current system time to the RTC module

    $ sudo hwclock -w
    
    
Check if the time was stored correctly on your hardware clock:

    $ sudo hwclock -r
    Tue Feb  9 17:17:46 2016  .375681 seconds
    
    

Step 5 Add autostart script to systemd
--------------------------------------
The final step is now to automatically initialize the hardware clock module 
and sync the system time to it. This tutorial assumes that you are using 
a systemd based Linux distribution for your Raspberry Pi, e.g. raspbian. 
Non-systemd based distribution require a different setup. 
* Store a script called `PirateClock.sh`in `/usr/bin`:

        $ sudo vim /usr/bin/PirateClock.sh 


* Enter the following code and save the file: 

        #! /bin/bash
        echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
        hwclock -s
    
    
* Adjust the file permissions on `/usr/bin/PirateClock.sh`

        $ sudo chmod +x /usr/bin/PirateClock.sh


* Now we need to create a "service"-script in the systemd architecture to call our script on every reboot. 

        $ sudo vim /lib/systemd/system/PirateClock.service
        
        
* Enter the following code and save the file:

        [Unit]
        Description=PirateClock - hwclock initialization and sync
        After=multi-user.target

        [Service]
        Type=idle
        ExecStart=/usr/bin/PirateClock.sh
        
        [Install]
        WantedBy=multi-user.target


* Adjust the file permissions on `/lib/systemd/system/PirateClock.service`

        $ sudo chmod 644 /lib/systemd/system/PirateClock.service
        
        
* Now we need to introduce systemd to the new "service": 

        $ sudo systemctl daemon-reload
        $ sudo systemctl enable PirateClock.service
        Created symlink from /etc/systemd/system/multi-user.target.wants/PirateClock.service 
        to /usr/lib/systemd/system/PirateClock.service.

        
* `reboot` your PirateBox and check if the script was correctly executed: 

        $ sudo systemctl status PirateClock.service
        [sudo] password for alarm: 
        * PirateClock.service - PirateClock - hwclock initialization and sync
           Loaded: loaded (/usr/lib/systemd/system/PirateClock.service; enabled; vendor preset: disabled)
           Active: inactive (dead) since Tue 2016-02-09 17:34:32 UTC; 49s ago
          Process: 204 ExecStart=/usr/bin/PirateClock.sh (code=exited, status=0/SUCCESS)
         Main PID: 204 (code=exited, status=0/SUCCESS)
        
        Feb 09 17:22:34 alarmpi systemd[1]: Started PirateClock - hwclock initializ...c.
        Hint: Some lines were ellipsized, use -l to show in full.

        $ sudo hwclock -r
        Tue Feb  9 17:35:57 2016  .465668 seconds
        $ date 
        Tue Feb  9 17:36:01 UTC 2016
        
        